import border from './borders';

export default function () {
    let btnMoreNews = $('.js-btn-more');

    btnMoreNews.on('click', function () {
        let ths = $(this);
        let wrapper = ths.parent();
        let list = wrapper.find('.js-scroll-list');
        let items = wrapper.find('.js-scroll-item-hidden');
        console.log(list)
        let height = list.height();
        list.css('height', height + 'px');
        ths.addClass('js-hidden-1');
        items.removeClass('js-scroll-item-hidden');
        list.mCustomScrollbar({
            autoDraggerLength: false,
            axis: 'y'
        });

        let bordered = list.find('.js-border');
        if (bordered.length > 0) {
            setTimeout(function () {
                border(true);
                if ($(window).width() > 1200) {
                    border(true);
                }
            }, 1000)
        }
    });
}