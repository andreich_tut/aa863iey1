import {videoScreen} from './animation';
import {jsVideoPlay} from '../../video';

export default function () {
    let isStart = $('#start').length > 0;
    if (isStart) {
        let start = new Promise((resolve) => {
            if (isStart) resolve('go');
        });

        start
            .then(function (go) {
                videoScreen();
                return go;
            })
            .then(function (go) {
                setTimeout(jsVideoPlay, 1650);
                return go;
            })
            .catch();
    }
}