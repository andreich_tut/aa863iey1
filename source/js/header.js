export default function () {
    let isHeader = $('#header').length > 0;

    if (isHeader) {
        let headerMenu = $('.header-menu');

        $('.js-burger').on('click', function () {
            $(this).addClass('open');
            setTimeout(function () {
                headerMenu.addClass('active');
            }, 150);
        });

        $(document).mouseup(function (e) {
            if (headerMenu.has(e.target).length === 0) headerMenu.removeClass('active');
            $('.js-burger').removeClass('open');
        });

        let canvas = document.querySelector('.js-canvas-logo');
        let ctx = canvas.getContext('2d');
        let size = 46;
        let center = size / 2 - 1;
        let radius = 20;
        let gradient = ctx.createLinearGradient(0, 0, size, size);
        gradient.addColorStop(0, '#3ea7a1');
        gradient.addColorStop(1, '#666dc8');

        ctx.canvas.width = size;
        ctx.canvas.height = size;
        ctx.strokeStyle = gradient;
        ctx.lineWidth = 2;

        ctx.beginPath();
        ctx.arc(center, center, radius, 0, 2 * Math.PI, false);
        ctx.stroke();
    }
}