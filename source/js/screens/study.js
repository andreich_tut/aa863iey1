import {clickVideo} from '../video';
import scrollByClick from '../scrollByClick';

export default function () {
    let isStudy = $('#study').length > 0;

    if (isStudy) {
        clickVideo();
        scrollByClick();
    }
}