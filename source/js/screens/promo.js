import {clickVideo} from '../video';
import scrollByClick from '../scrollByClick';

export default function () {
    let isPromo = $('#promo').length > 0;
    if (isPromo) {
        $('.js-copy-ref').on('click', function () {
            let $temp = $('<input>');
            $('body').append($temp);
            $temp.val($('.js-ref-link').attr('value')).select();
            document.execCommand('copy');
            $temp.remove();
        });

        $('.js-copy-post').on('click', function () {
            let $temp = $('<input>');
            let wrapper = $(this).closest('.promo__ref_item');
            let desc = wrapper.find('.promo__ref_post-desc').text();
            let link = wrapper.find('iframe').attr('src');
            $('body').append($temp);
            $temp.val(desc + ' ' + link).select();
            document.execCommand('copy');
            $temp.remove();
        });

        $('.js-copy-post-ref').on('click', function () {
            let $temp = $('<input>');
            let wrapper = $(this).closest('.promo__social_item');
            let desc = wrapper.find('.promo__social_item-text').text();
            let link = wrapper.find('iframe').attr('src');
            $('body').append($temp);
            $temp.val(desc + ' ' + link).select();
            document.execCommand('copy');
            $temp.remove();
        });

        clickVideo();
        scrollByClick();
    }
}