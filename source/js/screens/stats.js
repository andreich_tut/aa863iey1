export default function () {
    let isStats = $('#stats').length > 0;
    if (isStats) {
        let block1 = $('.stats__structure');
        let block2 = $('.stats__cash');
        let block3 = $('.stats__users');
        let tl = new TimelineLite();
        let tlOpt = {
            block1: {
                ease: Expo.easeOut,
                opacity: 0,
                y: -150
            },
            block2: {
                ease: Expo.easeOut,
                opacity: 0,
                x: 100
            },
            block3: {
                ease: Expo.easeOut,
                opacity: 0,
                y: 100
            }
        };

        tl
            .from(block1, 1, tlOpt.block1)
            .from(block2, 0.5, tlOpt.block2, '-=0.5')
            .from(block3, 0.5, tlOpt.block3, '-=0.25');
    }
}