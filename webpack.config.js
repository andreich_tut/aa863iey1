'use strict';

const path = require('path');
const webpack = require('webpack');
const html = require('html-webpack-plugin');
const merge = require('webpack-merge');
const devserver = require('./webpack/devserver');
const sass = require('./webpack/sass');
const css = require('./webpack/css');
const babel = require('./webpack/babel');
const extractCSS = require('./webpack/css.extract');
const files = require('./webpack/files');
const pug = require('./webpack/pug');
const jsProd = require('./webpack/js.prod');
const progressBar = require('progress-bar-webpack-plugin');

const PATHS = {
    source: path.join(__dirname, 'source'),
    build: path.join(__dirname, 'build')
};

const common = merge([
    {
        entry: {
            'index': PATHS.source + '/index.js',
        },
        output: {
            path: PATHS.build,
            filename: 'js/[name].js'
        },
        devtool: '#cheap-module-source-map',
        plugins: [
            new html({
                filename: 'index.html',
                chunks: 'index',
                template: PATHS.source + '/index.pug'
            }),
            new html({
                filename: 'start.html',
                chunks: 'index',
                template: PATHS.source + '/screens/start.pug'
            }),
            new html({
                filename: 'login.html',
                chunks: 'index',
                template: PATHS.source + '/screens/login.pug'
            }),
            new html({
                filename: 'personal.html',
                chunks: 'index',
                template: PATHS.source + '/screens/personal/personal.pug'
            }),
            new html({
                filename: 'partners.html',
                chunks: 'index',
                template: PATHS.source + '/screens/partners/partners.pug'
            }),
            new html({
                filename: 'study.html',
                chunks: 'index',
                template: PATHS.source + '/screens/study/study.pug'
            }),
            new html({
                filename: 'promo.html',
                chunks: 'index',
                template: PATHS.source + '/screens/promo/promo.pug'
            }),
            new html({
                filename: 'stats.html',
                chunks: 'index',
                template: PATHS.source + '/screens/stats/stats.pug'
            }),
            new html({
                filename: 'reward.html',
                chunks: 'index',
                template: PATHS.source + '/screens/reward/reward.pug'
            }),
            new html({
                filename: 'settings.html',
                chunks: 'index',
                template: PATHS.source + '/screens/settings/settings.pug'
            }),
            new webpack.ProvidePlugin({
                $: 'jquery',
                jQuery: 'jquery'
            }),
            new progressBar(),
        ],
        resolve: {
            alias: {
                'TweenLite': 'gsap/src/minified/TweenLite.min.js',
                'TimelineLite': 'gsap/src/minified/TimelineLite.min.js',
                'CSSPlugin': 'gsap/src/minified/plugins/CSSPlugin.min.js',
                'EasePack': 'gsap/src/minified/easing/EasePack.min.js'
            },
        }
    },
    babel(),
    files(),
    pug()
]);

module.exports = function (env) {
    if (env === 'production') {
        return merge([
            common,
            extractCSS(),
            jsProd()
        ]);
    }
    if (env === 'development') {
        return merge([
            common,
            devserver(),
            css(),
            sass(),
        ]);
    }
};