import scrollByClick from '../scrollByClick';

export default function () {
    let isSettings = $('#settings').length > 0;

    if (isSettings) {
        $('.js-edit-option').on('click', function () {
            let ths = $(this);
            let wrapper = ths.closest('.user__option');
            let editArea = wrapper.find('.js-edit-area');

            if (!editArea.hasClass('active')) {
                $('.js-edit-area').removeClass('active');
                editArea.addClass('active');
            } else {
                editArea.removeClass('active');
            }
        });

        let block1 = $('.user');
        let block2 = $('.settings__friends');
        let block3 = $('.settings__chat');
        let tl = new TimelineLite();
        let tlOpt = {
            block1: {
                ease: Expo.easeOut,
                opacity: 0,
                y: -150
            },
            block2: {
                ease: Expo.easeOut,
                opacity: 0,
                x: 100
            },
            block3: {
                ease: Expo.easeOut,
                opacity: 0,
                y: 100
            }
        };

        tl
            .from(block1, 1, tlOpt.block1)
            .from(block2, 0.5, tlOpt.block2, '-=0.5')
            .from(block3, 0.5, tlOpt.block3, '-=0.25');
        scrollByClick()
    }
}