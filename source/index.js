//styles
import './index.scss';

//vendors
import 'normalize.css';
import './vendors/particles.js';
import mCustomScrollbar from 'malihu-custom-scrollbar-plugin';

import TweenLite from 'TweenLite';
import TimelineLite from 'TimelineLite';
import CSSPlugin from 'CSSPlugin';
import EasePack from 'EasePack';

//helpers
import forEachPolyfill from './js/helpers'
import Promise from 'promise-polyfill';

import border from './js/borders';
import buttonClick from './js/ButtonClick';

//screens and parts
import Header from './js/header';
import Start from './js/screens/start/start';
import Login from './js/screens/login/login';
import Personal from './js/screens/personal';
import Partners from './js/screens/partners';
import Study from './js/screens/study';
import Promo from './js/screens/promo';
import Stats from './js/screens/stats';
import Reward from './js/screens/reward';
import Settings from './js/screens/settings';

//PromisePolyfill init
if (!window.Promise) {
    window.Promise = Promise;
}
//forEach polyfill init
forEachPolyfill();

//init helpers
buttonClick();

//init screens and parts
Header();
Start();
Login();
Personal();
Partners();
Study();
Promo();
Stats();
Reward();
Settings();

let isntStart = $('#start').length === 0;
if (isntStart && $(window).width() > 768) {
    particlesJS.load('lk-particles-main', 'https://andreich.pw/dev/aa863iey1/js/particles.json');
}

// GLOB borders init
// few for hover
let jsBorderOn = $('.js-border').length !== 0;
if (jsBorderOn) {
    setTimeout(function () {
        border();
        if ($(window).width() > 1200) {
            border();
        }
    }, 100)
}







