export default function (secondary = false) {
    let item = document.querySelectorAll('.js-border');
    let shift = 1;

    function drawBorder(resize = false) {
        item.forEach(function (item) {
            let ths = item;
            let width = ths.offsetWidth;
            let height = ths.offsetHeight;
            let childrens = ths.childNodes;
            let firstChild = childrens[0];
            let secondChild = childrens[1];

            if (resize === false) {
                let canvasCreated = document.createElement('canvas');
                canvasCreated.classList.add('border');
                ths.insertBefore(canvasCreated, firstChild);
            } else {
                secondChild.width = width;
                secondChild.height = height;
            }

            let canvas = ths.firstChild;
            let ctx = canvas.getContext('2d');
            let colorFrom = ths.dataset.colorFrom;
            let colorTo = ths.dataset.colorTo;
            let radius = (ths.dataset.radius) ? ths.dataset.radius : 9;

            //coords
            let x = width - shift;
            let y = height - shift;
            let r = radius;

            if (item.classList.contains('js-hidden')) {
                item.classList.add('bordered');
            }

            let gradient = ctx.createLinearGradient(shift, shift, x, y);
            gradient.addColorStop(0, colorFrom);
            gradient.addColorStop(1, colorTo);

            ctx.canvas.width = width;
            ctx.canvas.height = height;

            ctx.strokeStyle = gradient;
            ctx.lineWidth = 2;

            ctx.clearRect(-2, -2, width + 2, height + 2);
            ctx.beginPath();
            ctx.moveTo(r, shift);
            ctx.lineTo(x - r, shift);
            ctx.arcTo(x, shift, x, y, r);
            ctx.lineTo(x, y - r);
            ctx.arcTo(x, y, x - r, y, r);
            ctx.lineTo(r, y);
            ctx.arcTo(shift, y, shift, y - r, r);
            ctx.lineTo(shift, r);
            ctx.arcTo(shift, shift, r, shift, r);
            ctx.stroke();
        });
    }

    setTimeout(function () {
        drawBorder();

        window.onresize = function () {
            drawBorder(true);
        };
    }, 0);
}