import {loadParticles} from './screens/start/animation';

let video = document.getElementById('ytplayer');

function clickVideo() {
    let btn = document.querySelectorAll('.js-video-start');
    let tlOpt = {
        elem: {
            opacity: 0,
            y: -20
        },
        overlay: {opacity: 0}
    }

    btn.forEach(function (item) {
        item.addEventListener('click', function (e) {
            let ths = e.target;
            let wrapper = ths.parentNode.parentNode.parentNode;
            let titleWrapper = wrapper.querySelector('.js-video-title');
            let title = wrapper.querySelector('.js-video-title').children;
            let overlay = wrapper.querySelector('.js-video-overlay');
            let curBtn = wrapper.querySelector('.js-video-start');
            let hiddenCl = 'js-hidden';
            let timeLine = new TimelineLite();
            console.log(wrapper, title, overlay);

            timeLine
                .to(curBtn, 0.25, tlOpt.elem)
                .staggerTo(title, 0.25, tlOpt.elem, 0.15)
                .to(overlay, 1, tlOpt.overlay, '-=0.8');

            setTimeout(function () {
                ths.classList.add(hiddenCl);
                titleWrapper.classList.add(hiddenCl);
                overlay.classList.add(hiddenCl);
            }, 850);
        });
    });
}

function jsVideoPlay() {
    let wrapper = video.parentNode;
    let height = wrapper.offsetHeight;
    let player;
    let videoSettings = {
        height: height,
        width: '100%',
        videoId: 'ZZmHD8gGgmI',
        playerVars: {
            controls: 0,
            showinfo: 0,
            modestbranding: 1
        },
        events: {
            'onReady': init,
        }
    }

    function doYT() {
        window.onYouTubePlayerAPIReady = function () {
            player = new YT.Player('ytplayer', videoSettings);
        }
    }

    window.YT && doYT() || function () {
        let script = document.createElement('script');
        script.setAttribute('type', 'text/javascript');
        script.setAttribute('src', 'https://www.youtube.com/player_api');
        script.onload = doYT;
        script.onreadystatechange = function () {
            if (this.readyState == 'complete' || this.readyState == 'loaded') doYT()
        };
        (document.getElementsByTagName('head')[0] || document.documentElement).appendChild(script)
    }();

    function init() {
        let btn = document.querySelector('.js-video-start');
        loadParticles();
        clickVideo();

        btn.addEventListener('click', function () {
            player.playVideo();
        });
    }
}

export {jsVideoPlay, clickVideo}