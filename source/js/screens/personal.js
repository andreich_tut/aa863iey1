import scrollByClick from '../scrollByClick';
export default function () {
    let isPersonal = $('#personal').length > 0;

    if (isPersonal) {
        let news = $('.personal__news');
        let cash = $('.personal__cash');
        let study = $('.personal__study');
        let players = $('.personal__players');
        let tl = new TimelineLite();
        let tlOpt = {
            news: {
                ease: Expo.easeOut,
                opacity: 0,
                y: -150
            },
            cash: {
                ease: Expo.easeOut,
                opacity: 0,
                x: 100
            },
            study: {
                ease: Expo.easeOut,
                opacity: 0,
                y: 100
            },
            players: {
                ease: Expo.easeOut,
                opacity: 0,
                x: -100
            }
        };

        tl
            .from(news, 1, tlOpt.news)
            .from(cash, 0.5, tlOpt.cash, '-=0.5')
            .from(study, 0.5, tlOpt.study, '-=0.25')
            .from(players, 0.5, tlOpt.players, '-=0.25');

        scrollByClick();
    }
}