export default function () {
    let login = $('.login');
    let title = $('.login__title');
    let label = $('.login__label');
    let termsW = $('.login__terms-wrapper');
    let checkbox = $('.login__checkbox-wrapper');
    let btn = $('.main-btn');
    let tl = new TimelineLite();
    let tlOpt = {
        login: {
            opacity: 0,
            y: -150
        },
        x: {
            opacity: 0,
            x: -20
        },
        y: {
            opacity: 0,
            y: -50
        }
    };
    tl.from(login, 0.5, tlOpt.login)
        .from(btn, 0.5, tlOpt.y, '-=0.1')
        .from(title, 0.2, tlOpt.x, '-=0.1')
        .staggerFrom(label, 0.2, tlOpt.x, 0.05, '-=0.1')
        .from(termsW, 0.2, tlOpt.x, '-=0.1')
        .from(checkbox, 0.2, tlOpt.x, '-=0.1');
}