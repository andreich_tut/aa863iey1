import animation from './animation';

export default function () {
    let isLogin = $('#login').length > 0;

    if (isLogin) {
        // animation();
        let check = $('.js-checkbox');
        check.on('click', function () {
            let ths = $(this);
            let img = ths.find('img');
            let input = ths.find('input');
            img.toggleClass('active');
            if (input.is(':checked')) input.attr('checked', false);
            else input.attr('checked', true);
        });

        let terms = $('.login__terms');

        terms.mCustomScrollbar({autoDraggerLength: false, axis: 'y'});
    }
}
