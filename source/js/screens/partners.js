import scrollByClick from '../scrollByClick';
export default function () {
    let isPartners = $('#partners').length > 0;

    if (isPartners) {
        let search = $('.partners__search');
        let friends = $('.partners__friends');
        let games = $('.partners__games');
        let tl = new TimelineLite();
        let tlOpt = {
            search: {
                ease: Expo.easeOut,
                opacity: 0,
                y: -150
            },
            friends: {
                ease: Expo.easeOut,
                opacity: 0,
                x: 100
            },
            games: {
                ease: Expo.easeOut,
                opacity: 0,
                y: 100
            }
        };

        tl
            .from(search, 1, tlOpt.search)
            .from(friends, 0.5, tlOpt.friends, '-=0.5')
            .from(games, 0.5, tlOpt.games, '-=0.25');

        scrollByClick();
    }
}