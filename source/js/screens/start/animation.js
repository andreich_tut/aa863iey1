function videoScreen() {
    let timeLine = new TimelineLite();
    let video = $('.js-video');
    let overlay = $('.js-video-overlay');
    let titleStroke = video.find('span');
    let videoBtn = $('.js-video-start');
    let mainBtn = $('.main-btn');
    let stage = {
        video: {
            opacity: 0,
            y: -150
        },
        btnPlay: {
            opacity: 0,
            scale: 0.7,
            y: -20
        },
        title: {
            opacity: 0,
            y: -30
        },
        overlay: {
            opacity: 0
        }
    };

    timeLine
        .from(video, 0.7, stage.video)
        .staggerFrom(titleStroke, 0.35, stage.title, 0.25)
        .from(videoBtn, 0.35, stage.btnPlay, '-=0.1')
        .from(mainBtn, 0.35, stage.title, '-=0.15');
}

function destroyFirstParticle() {
    if ($(window).width() < 768) {
        if (window["pJSDom"] instanceof Array && window["pJSDom"].length > 0) {
            window["pJSDom"][0].pJS.fn.vendors.destroypJS();
        }
    }
}

function loadParticles() {
    if ($(window).width() > 768) {
        particlesJS.load('lk-particles-1', 'https://andreich.pw/dev/aa863iey1/js/particles.json');
    }
    particlesJS.load('lk-particles-2', 'https://andreich.pw/dev/aa863iey1/js/particles.json');

    window.onresize = destroyFirstParticle;

    let particles = ('.particles');
    TweenLite.from(particles, 2, {ease: Expo.easeOut, opacity: 0 }, 0.5);
}

export {videoScreen, loadParticles}