export default function () {
    let btn = $('.main-btn');

    btn.on('mousedown', function () {
        let ths = $(this);
        TweenLite.to(ths, 0.15, {y: 5});
    });

    btn.on('mouseup', function () {
        let ths = $(this);
        TweenLite.to(ths, 0.15, {y: 0});
    });
}