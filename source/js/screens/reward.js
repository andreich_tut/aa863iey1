import scrollByClick from '../scrollByClick';

export default function () {
    let isBlock = $('#reward').length > 0;
    if (isBlock) {
        let block1 = $('.reward__balance');
        let block2 = $('.reward__history');
        let block3 = $('.reward__bonus');
        let tl = new TimelineLite();
        let tlOpt = {
            block1: {
                ease: Expo.easeOut,
                opacity: 0,
                y: -150
            },
            block2: {
                ease: Expo.easeOut,
                opacity: 0,
                x: 100
            },
            block3: {
                ease: Expo.easeOut,
                opacity: 0,
                y: 100
            }
        };

        tl
            .from(block1, 1, tlOpt.block1)
            .from(block2, 0.5, tlOpt.block2, '-=0.5')
            .from(block3, 0.5, tlOpt.block3, '-=0.25');
        scrollByClick();
    }
}